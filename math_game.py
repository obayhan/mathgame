# -*- coding: utf-8 -*-
from game_table import  GameTable
from settings import Settings

__author__ = 'ozgur'
__creation_date__ = '1/24/15' '5:57 PM'
import pygame as pg


if __name__ == '__main__':
    # call with width of window and fps
    settings = Settings()
    pg.mixer.pre_init(44100, -16, 2, 2048)
    while True:
        GameTable(settings).run()