# -*- coding: utf-8 -*-
import os

__author__ = 'ozgur'
__creation_date__ = '1/24/15' '5:52 PM'

import pygame as pg


class StaticSettings():
    def __init__(self):
        self.COLOR_BACKGROUND = (47, 47, 47)
        self.COLOR_CELL_BORDER = (94, 202, 20)
        self.COLOR_PRIMARY = (42, 139, 202)
        self.COLOR_ACTIVE_CELL = (217, 53, 79)  # d9534f
        self.COLOR_ACTIVE_RC = (247, 76, 12)  # F7760C
        self.COLOR_MENU = (252, 126, 0)
        self.MAX_STRING_LENGTH = 9
        self.SOUND_BG = os.path.join("sounds", "Night_Sounds_-_Crickets-Lisa_Redfern.wav")
        self.SOUND_OK = os.path.join("sounds", "glass-ping.wav")
        self.SOUND_FAIL = os.path.join("sounds", "Sad_Trombone-Joe_Lamb.wav")
        self.NUMERIC_KEYS = [
            pg.K_0,
            pg.K_1,
            pg.K_2,
            pg.K_3,
            pg.K_4,
            pg.K_5,
            pg.K_6,
            pg.K_7,
            pg.K_8,
            pg.K_9,
            pg.K_KP0,
            pg.K_KP1,
            pg.K_KP2,
            pg.K_KP3,
            pg.K_KP4,
            pg.K_KP5,
            pg.K_KP6,
            pg.K_KP7,
            pg.K_KP8,
            pg.K_KP9,
        ]


class Settings():
    def __init__(self):
        self.FPS = 20
        self.WINDOW_WIDTH = 800
        self.WINDOW_HEIGHT = 800
        self.LANGUAGE = 'tr'