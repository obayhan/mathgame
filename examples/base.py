# -*- coding: utf-8 -*-  
__author__ = 'ozgur'
__creation_date__ = '1/12/15' '11:17 PM'
import pygame as pg
from pygame import Surface, display, time
import random
import math
# Bazı renkleri tanımla
black = ( 0, 0, 0)
white = ( 255, 255, 255)
red = ( 255, 0, 0)
# Bu sınıf topu temsil ediyor
# Pygame içerisindeki "Sprite" sınıfından türüyor


class Block(pg.sprite.Sprite):
    def __init__(self, color, width, height):
        # Üst sınıfın (Sprite) yapıcı fonksiyonunu çağır
        super(Block, self).__init__()  # Bloğun bir resmini oluştur ve onu bir renkle doldur.
        self.image = pg.Surface([width, height])
        self.image.fill(color)
        self.rect = self.image.get_rect()

    def update(self):
        # Bloğu 1 piksel aşağı oynat
        self.rect.y += 1

# Pygame'i başlat
pg.init()

myfont = pg.font.SysFont("monospace", 15)
# Ekranın yüksekliğini ve genişliğini ayarla
screen_width = 700
screen_height = 700
screen = display.set_mode([screen_width, screen_height])

# Bu 'sprite'ların bir listesi. Programdaki her blok
# bu listeye ekleniyor.
# Bu liste 'Group' denen bir sınıfla yönetiliyor.
block_list = pg.sprite.Group()
# Bu her sprite'ın listesi
# Bütün bloklar ve oyuncu bloğu da burada.
all_sprites_list = pg.sprite.Group()

for i in range(50):
    # Bu tek bir bloğu temsil ediyor
    block = Block(black, 20, 15)
    # Blok için rastgele konum belirleniyor
    block.rect.x = random.randrange(screen_width - 20)
    block.rect.y = random.randrange(screen_height - 15 - 20) + 20
    # Bloğu nesnelerin listesine ekliyor
    block_list.add(block)
    all_sprites_list.add(block)

player = Block(red, 20, 15)
all_sprites_list.add(player)

# Kullanıcı kapatma düğmesine basana kadar döngü
done = False
# Ekranın ne hızla güncelleneceğini yönetmek için kullanılır
clock = time.Clock()
score = 0
mx = 0
my = 0
distance = 0
# -------- Ana Program Döngüsü -----------
while done is False:
    for event in pg.event.get():  # Kullanıcı bir şey yaptı
        if event.type == pg.QUIT:  # Kullanıcı kapat butonuna bastıysa
            done = True  # Oyunun bittiğini ve döngüden çıkılmasını söyle
    # Ekranı temizle
    screen.fill(white)
    pos = pg.mouse.get_pos()
    player.rect.x = pos[0]
    player.rect.y = pos[1]

    # Kullanıcı bloğunun herhangi bir şeyle çarpışıp çarpışmadığını kontrol et
    blocks_hit_list = pg.sprite.spritecollide(player, block_list, True)
    # Çarpışma listesini kontrol et.
    if len(blocks_hit_list) > 0:
        score += len(blocks_hit_list)

    # ScoreBorad
    if mx is 0 and my is 0:
        mx = pos[0]
        my = pos[1]
        distance = 0
    else:
        if score < 50:
            distance += int(math.sqrt((math.fabs(pos[0] - mx) * math.fabs(pos[0] - mx)) + (math.fabs(pos[1] - my) * math.fabs(pos[1] - my))))
            mx = pos[0]
            my = pos[1]
    label = myfont.render("Adet : " + unicode(score) + " Mesafe: " + unicode(distance) + " px.", 1, red)
    screen.blit(label, (20, 0))
    # Tüm spriteları çiz
    all_sprites_list.draw(screen)
    # Saniyede 20 frame (20 FPS) ile sınırla
    clock.tick(20)
    # İlerle ve ekranı çizdirdiğimiz son hali ile güncelle
    pg.display.flip()
pg.quit()