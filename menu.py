# -*- coding: utf-8 -*-  
__author__ = 'ozgur'
__creation_date__ = '1/24/15' '6:00 PM'

import pygame as pg
from pygame.time import Clock as pg_Clock
from pygame import Surface as pg_Surface, event as pg_event, draw as pg_draw
from pygame.font import SysFont as pg_SysFont
from pygame.mixer import music as pg_music, Sound as pg_Sound

import random
from random import randint
from copy import copy


class Menu(object):
    def __init__(self, settings):
        """Initialize pygame, window, background, font,...
           default arguments
        """
        # Constants
        self.s = settings
        self.table_color = self.s.COLOR_BACKGROUND
        self.fps = self.s.FPS
        # Init variables

        # Init Table
        pg.init()
        pg.display.set_caption(u"Matematik Oyunu")
        self.width = self.s.WINDOW_WIDTH
        self.height = self.s.WINDOW_HEIGHT
        self.screen = pg.display.set_mode((self.width, self.height), pg.DOUBLEBUF)
        self.background = pg_Surface(self.screen.get_size()).convert()
        self.background.fill(self.table_color)
        self.clock = pg_Clock()
        self.font = pg_SysFont('mono', 18, bold=True)

        # init sounds
        self.sound_bg = pg_Sound(self.s.SOUND_BG)
        self.sound_bg.play(-1)

        self.active_item = 0

        self.menu_list = [
            "Kolay",
            "Orta",
            "Zor",
            "Çılgın",
            "Ayarlar",
            "Çıkış",
        ]

    def draw_menu(self):
        i = 0
        for menu_item in self.menu_list:

            i+=1

    def run(self):
        """The mainloop
        """
        # self.paint()
        running = True
        while running:
            self.background.fill(self.table_color)
            self.draw_menu()
            for event in pg_event.get():
                if event.type == pg.QUIT:
                    running = False
                elif event.type == pg.KEYDOWN:
                    if event.key == pg.K_ESCAPE:
                        return False
                        # elif event.key in s.NUMERIC_KEYS and len(self.active_cell.temp_value) < s.MAX_STRING_LENGTH:
                        # self.active_cell.temp_value += event.unicode
                        # elif (event.key == pg.K_BACKSPACE or event.key == pg.K_DELETE) and len(self.active_cell.temp_value) > 0:
                        # self.active_cell.temp_value = self.active_cell.temp_value[:-1]
                        # elif event.key == pg.K_KP_ENTER or event.key == pg.K_RETURN:
                        # if self.active_cell.temp_value == self.active_cell.value:
                        # self.active_cell.isactive = False
                        #         self.active_cell.temp_value = None
                        #         self.save_old_cell()
                        #     else:
                        #         self.failed = True
                        #         self.sound_fail.play()

            self.clock.tick(self.fps)
            pg.display.flip()
            self.screen.blit(self.background, (0, 0))

        pg.quit()

